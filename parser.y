%{
#include <stdio.h>
#include "main.h"
%}

/* Declaração dos tokens da gramática da Linguagem K */
%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_LIT_INT
%token TK_LIT_FLOAT
%token TK_LIT_FALSE
%token TK_LIT_TRUE
%token TK_LIT_CHAR
%token TK_LIT_STRING
%token TK_IDENTIFICADOR
%token TOKEN_ERRO

%%
 /* Regras (e ações) da gramática da Linguagem K */

primary: body {printf("SUCESSO!\n"); return IKS_SYNTAX_SUCESSO;};

body: body declaration | body function | declaration | function;

type: TK_PR_INT | TK_PR_FLOAT | TK_PR_CHAR | TK_PR_STRING;

declaration: type ':' TK_IDENTIFICADOR ';' {printf("Is declaration!\n");}| type ':' TK_IDENTIFICADOR '[' TK_LIT_INT  ']' ';';

function: type ':' TK_IDENTIFICADOR args block {printf("Is function!\n");};

args: '(' ')' | '(' list_params ')';

list_params: list_params ',' param | param;

param : type ':' TK_IDENTIFICADOR ';';

block: '{' '}' | '{' commands '}';

commands: commands simple_command | simple_command;

simple_command: TK_PR_RETURN exp ';'| assignment;

assignment: TK_IDENTIFICADOR '=' exp ';'| TK_IDENTIFICADOR '[' exp ']' '=' exp ';';

/*Expressoes*/
exp: TK_LIT_INT;
%%


