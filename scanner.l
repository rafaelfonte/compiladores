%{
#include "parser.h"
/*#include "tokens.h"*/
extern void incLine(char * input);
extern void add_entry_symbol_table(int TOKEN, char * identifier, char * literal);
void checkTokens(int tk);
int count = 0;
%}

DIGITO [0-9]
UM_A_NOVE [1-9]
LETRA [a-z]|[A-Z]
CARACTER [a-z]|[A-Z]|"_"

%%



(("-"))?(({DIGITO}+"."{DIGITO}+)|("0."{DIGITO})) { checkTokens(TK_LIT_FLOAT); return TK_LIT_FLOAT;}

(("-"))?({DIGITO}+) { checkTokens(TK_LIT_INT); return TK_LIT_INT;}

"true"  { checkTokens(TK_LIT_TRUE); return TK_LIT_TRUE;}
"false" { checkTokens(TK_LIT_FALSE); return TK_LIT_FALSE;}

"\""(\\.|[^"\""])*"\"" { checkTokens(TK_LIT_STRING); return TK_LIT_STRING;}
"\'"(.)"\'" { checkTokens(TK_LIT_CHAR); return TK_LIT_CHAR;}



"<=" { return TK_OC_LE; }
">=" { return TK_OC_GE; }
"==" { return TK_OC_EQ; }
"!=" { return TK_OC_NE; }
"&&" { return TK_OC_AND; }
"||" { return TK_OC_OR; }

","|";"|":"|"("|")"|"["|"]"|"{"|"}"|"+"|"-"|"*"|"/"|"<"|">"|"="|"!"|"&"|"$" { return (int)yytext[0];}

int {checkTokens(TK_PR_INT); return TK_PR_INT; }
float {checkTokens(TK_PR_FLOAT); return TK_PR_FLOAT; }
bool {checkTokens(TK_PR_BOOL); return TK_PR_BOOL; }
char {checkTokens(TK_PR_CHAR); return TK_PR_CHAR; }
string {checkTokens(TK_PR_STRING); return TK_PR_STRING; }
if { return TK_PR_IF; }
then { return TK_PR_THEN; }
else { return TK_PR_ELSE; }
while { return TK_PR_WHILE; }
do { return TK_PR_DO; }
input { return TK_PR_INPUT; }
output { return TK_PR_OUTPUT; }
return { return TK_PR_RETURN; }

{CARACTER}+({CARACTER}|{DIGITO})* {checkTokens(TK_IDENTIFICADOR); return TK_IDENTIFICADOR;}

[ \t\n]* { incLine(yytext); }

"/*"(.|\n)*"*/" { incLine(yytext); }

"//".*"\n" { incLine(yytext); }

. { return TOKEN_ERRO; }
%%

int token[3];
char identifier[256];

void checkTokens(int tk){
/*	if(isType(tk) && count == 0){
		count++;
	}
	if(isIdentifier(tk) && count == 1){
		count++;
	}
	if(isLiteral(tk) && count == 2){
		if(isType(token[0]) && isIdentifier(token[1]) && isLiteral(token[2])){
			
			
		}
	}
	//add_entry_symbol_table(int TOKEN, char * identifier, char * literal);
*/
}

int isType(int tk){
	return (tk == TK_PR_INT)||(tk == TK_PR_FLOAT)||(tk == TK_PR_BOOL)||(tk == TK_PR_CHAR)||(tk == TK_PR_STRING);
}

int isIdentifier(int tk){
	return tk == TK_IDENTIFICADOR;
}

int isLiteral(int tk){
	return 1;
}












