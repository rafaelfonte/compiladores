#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct comp_list{
	int id;
	void * data;
	struct comp_list * next;
	struct comp_list * prev;
}comp_list_t;



comp_list_t * create_list(comp_list_t * new_list);
comp_list_t * insert_element_list(comp_list_t * list, void * data, int new_id);
comp_list_t * remove_element_list(comp_list_t * list, int id);
comp_list_t * lookup_element_list(int searched_id,comp_list_t * lst);
comp_list_t * concat_lists(comp_list_t * l1, comp_list_t * l2);
void delete_list(comp_list_t * lst);
int ordering_list();


