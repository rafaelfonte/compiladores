#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define STR_SIZE 256



typedef struct{
	char identifier[STR_SIZE];
	int TOKEN;
	char literal[STR_SIZE];
	int lineNumber;
}comp_dict_item_t;


typedef struct comp_dict{
	comp_dict_item_t * dic_entry;
	struct comp_dict * next;	
}comp_dict_t;

comp_dict_t * create_dic_table();
comp_dict_t * add_entry(comp_dict_t * dictionary, char * entry_identifier, int TOKEN, char * lit, int lineNumber);
comp_dict_t * remove_entry(comp_dict_t * dictionary, char * entry_name);
comp_dict_t * lookup_entry(comp_dict_t * dictionary, char * entry_name);
comp_dict_t * entry_at(comp_dict_t * dictionary, int index);
void print_dictionary(comp_dict_t * dictionary);

