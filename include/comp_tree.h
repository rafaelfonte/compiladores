#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE_NAME 256

typedef struct comp_tree{
	void * data;
	struct comp_tree * list_of_children;
	struct comp_tree * next;
	int num_nodes;
	int id;
	char name[SIZE_NAME];
}comp_tree_t;

comp_tree_t * create_tree();
void insert_element_tree(comp_tree_t * branch, void * data, int id, char * name);
void remove_element_tree(comp_tree_t * branch, int id);
void delete_tree(comp_tree_t * t);
comp_tree_t * lookup_element_tree(char * name);
