#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define SIZE_NAME 256
#define GRAPH_OP_SUCCESSFUL 1
#define GRAPH_OP_UNSUCCESSFUL 0



typedef struct comp_graph_adjacent{
	void * element_ptr;
	struct comp_graph_adjacent * next;
}comp_graph_adjacent_t;

typedef struct comp_graph{
	void * data;
	int id;
	char name[SIZE_NAME];
	struct comp_graph * next;
	comp_graph_adjacent_t * list_adjacent_nodes;
}comp_graph_t;

comp_graph_t * create_graph();
comp_graph_t * insert_element_graph(comp_graph_t * graph_lst, void * data, int id, char * name);
comp_graph_t * remove_element_graph(comp_graph_t * graph_lst, char * name);
int connect_elements_graph(comp_graph_t * node, comp_graph_t * adj_node);
int remove_connection_graph(comp_graph_t * node, comp_graph_t * adj_node);
comp_graph_t * lookup_element_graph(int searched_id,comp_graph_t * graph_lst);
void delete_connections(comp_graph_adjacent_t * lst);

