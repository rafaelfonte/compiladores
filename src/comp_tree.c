#include "comp_tree.h"


comp_tree_t * create_tree(){
	comp_tree_t * new_tree = (comp_tree_t*) malloc(sizeof(comp_tree_t));
	new_tree->data = NULL;
	new_tree->list_of_children = NULL;
	new_tree->next = NULL;
	new_tree->num_nodes = 0;
	new_tree->id = -1;
	(new_tree->name)[0] = '\0';
	return new_tree;
}

void insert_element_tree(comp_tree_t * branch, void * data, int id, char * name){
	comp_tree_t * ptaux = branch->list_of_children,*prev;
	while(ptaux != NULL && ptaux->id < id){
		prev = ptaux;
		ptaux = ptaux->next;
	}
	comp_tree_t * new_node = (comp_tree_t*)malloc(sizeof(comp_tree_t));
	new_node->data = data;
	new_node->list_of_children = NULL;
	new_node->num_nodes = 0;
	new_node->id = id;
	strncpy(new_node->name,name,SIZE_NAME);	

	if(prev == NULL){
		branch->list_of_children = new_node;
	}
	else{
		prev->next = new_node;
	}
	new_node->next = ptaux;
}

void remove_element_tree(comp_tree_t * branch, int id){
	comp_tree_t * ptaux = branch->list_of_children,*prev;
	while(ptaux != NULL && ptaux->id != id){
		prev = ptaux;
		ptaux = ptaux->next;
	}
	if(ptaux != NULL){
		if(prev == NULL){
			branch->list_of_children = ptaux->next;
		}
		else{
			prev->next = ptaux->next;
		}
		free(ptaux->data);
		delete_tree(ptaux->list_of_children);
		free(ptaux);
	}
	else{
		printf("Error: could not find node with id \'%d\'\n", id);
	}
}


void delete_tree(comp_tree_t * t){
	free(t->data);
	if(t->next != NULL){
		delete_tree(t->next);	
	}
	delete_tree(t->list_of_children);
	free(t);
}

//TODO: to implement later...
comp_tree_t * lookup_element_tree(char * name){
	return NULL;

}
