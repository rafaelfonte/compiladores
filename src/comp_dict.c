#include "comp_dict.h"

comp_dict_t * create_dic_table(){
	comp_dict_t * new_dictionary = (comp_dict_t*) malloc(sizeof(comp_dict_t));
	new_dictionary->dic_entry = NULL;
	new_dictionary->next = NULL;
	
	return new_dictionary;
}

comp_dict_t * add_entry(comp_dict_t * dictionary, char * entry_identifier, int TOKEN, char * lit, int lineNumber){
	if(dictionary == NULL){printf("CASE new\n");
		printf("Error: dictionary has not been initialized...");
		return NULL;
	}
	
	if(dictionary->dic_entry == NULL){printf("CASE 1\n");
		dictionary->dic_entry = (comp_dict_item_t*) malloc(sizeof(comp_dict_item_t));
		strncpy(dictionary->dic_entry->identifier,entry_identifier,STR_SIZE);
		strncpy(dictionary->dic_entry->literal,lit,STR_SIZE);
		dictionary->dic_entry->lineNumber = lineNumber;
		dictionary->dic_entry->TOKEN = TOKEN;
	}
	else{printf("CASE 2\n");
		comp_dict_t * ptaux = dictionary;
		comp_dict_t * prev = NULL;
		while(ptaux != NULL && strcmp(entry_identifier,ptaux->dic_entry->identifier) > 0){
			prev = ptaux;
			ptaux = ptaux->next;
		}

		comp_dict_t * new_entry = (comp_dict_t*) malloc(sizeof(comp_dict_t));
		new_entry->dic_entry = (comp_dict_item_t*) malloc(sizeof(comp_dict_item_t));
		strncpy(new_entry->dic_entry->identifier,entry_identifier,STR_SIZE);
		strncpy(dictionary->dic_entry->literal,lit,STR_SIZE);
		dictionary->dic_entry->lineNumber = lineNumber;
		new_entry->dic_entry->TOKEN = TOKEN;

		if(prev == NULL){//Primeira entrada...
			new_entry->next = ptaux;
			return new_entry;
		}

		new_entry->next = ptaux;
		prev->next = new_entry;
	}
	return dictionary;
}

comp_dict_t * remove_entry(comp_dict_t * dictionary, char * entry_identifier){
	comp_dict_t * ptaux = dictionary,*aux_prev = NULL;
	
	if(dictionary == NULL || dictionary->dic_entry == NULL){//Caso nao esteja com a primeira entrada preenchida ou o dicionario nao foi inicializado...
		return NULL;
	}
	while(ptaux != NULL && strcmp(entry_identifier,ptaux->dic_entry->identifier) != 0){//Procura...
		aux_prev = ptaux;
		ptaux = ptaux->next;
	}
	if(ptaux == NULL){//Nao encontrou a entrada...
		return NULL;
	}
	if(aux_prev == NULL){//Caso seja o primeiro elemento...
		ptaux = dictionary->next;
		free(dictionary->dic_entry);
		free(dictionary);
		return ptaux;
	}
	
	aux_prev->next = ptaux->next;
	free(ptaux->dic_entry);
	free(ptaux);
	return dictionary;
}

comp_dict_t * lookup_entry(comp_dict_t * dictionary, char * entry_identifier){
	comp_dict_t * ptaux = dictionary;
	
	if(dictionary == NULL || dictionary->dic_entry == NULL){//Caso nao esteja com a primeira entrada preenchida ou o dicionario nao foi inicializado...
		return NULL;
	}
	while(ptaux != NULL && strcmp(entry_identifier,ptaux->dic_entry->identifier) != 0){//Procura...
		ptaux = ptaux->next;
	}
	return ptaux;
}

int lookup_entry_token(comp_dict_t * dictionary, char * entry_identifier){
	comp_dict_t * e = lookup_entry(dictionary,entry_identifier);
	return (e == NULL ? -1 : e->dic_entry->TOKEN);
}

comp_dict_t * entry_at(comp_dict_t * dictionary, int index){
	comp_dict_t * ptaux = dictionary;
	int iter = 0;
	
	if(dictionary == NULL || dictionary->dic_entry == NULL){//Caso nao esteja com a primeira entrada preenchida ou o dicionario nao foi inicializado...
		return NULL;
	}
	while(ptaux != NULL && iter != index){//Procura...
		ptaux = ptaux->next;
		iter++;
	}
	return ptaux;
}


void print_dictionary(comp_dict_t * dictionary){
	comp_dict_t * ptaux = dictionary;
	
	while(ptaux != NULL){
		printf("Type: %d\nId: %s\nLiteral: %s\n\n\n", ptaux->dic_entry->TOKEN,ptaux->dic_entry->identifier,ptaux->dic_entry->literal);
	}
}
