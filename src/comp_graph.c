#include "comp_graph.h"

comp_graph_t * create_graph(){
	comp_graph_t * new_graph = (comp_graph_t *)malloc(sizeof(comp_graph_t));
	new_graph->data = NULL;
	new_graph->id = -1;
	sprintf(new_graph->name,"");
	new_graph->list_adjacent_nodes = NULL;
	new_graph->next = NULL;
}

comp_graph_t * insert_element_graph(comp_graph_t * graph_lst, void * data, int id, char * name){

	comp_graph_t * ptaux = graph_lst, * prev = NULL;
	if(graph_lst == NULL){
		printf("Error: graph list has not been initialized!\n");
		return NULL;
	}
	if(graph_lst->id == -1 && graph_lst->next == NULL){//Grafo nao teve a primeira entrada preenchida ainda...
		graph_lst->data = data;
		graph_lst->id = id;
		strncpy(graph_lst->name,name,SIZE_NAME);
		return graph_lst;
	}
	while(ptaux->next != NULL && ptaux->id < id){
		prev = ptaux;
		ptaux = ptaux->next;
	}

	comp_graph_t * new_graph_node = (comp_graph_t *)malloc(sizeof(comp_graph_t));
	new_graph_node->data = data;
	new_graph_node->id = id;
	strncpy(graph_lst->name,name,SIZE_NAME);
	new_graph_node->list_adjacent_nodes = NULL;
	
	if(prev == NULL){//Caso estivermos na primeira entrada...
		new_graph_node->next = graph_lst;
		return new_graph_node;
	}
	
	new_graph_node->next = ptaux;
	prev->next = new_graph_node;	
	
	return graph_lst;
}

int connect_elements_graph(comp_graph_t * node, comp_graph_t * adj_node){
	if(node == NULL || adj_node == NULL){
		if(node == NULL){
			printf("Erro: nodo nulo para remocao!\n");
		}
		if(adj_node == NULL){
			printf("Erro: nodo adjacente nulo para remocao!\n");
		}
		return GRAPH_OP_UNSUCCESSFUL;
	}
	else{
		comp_graph_adjacent_t * ptaux = node->list_adjacent_nodes,*prev = NULL;
		while(ptaux != NULL && ((comp_graph_t*)ptaux->element_ptr)->id < adj_node->id){
			prev = ptaux;
			ptaux = ptaux->next;
		}
		comp_graph_adjacent_t * new_connection = (comp_graph_adjacent_t*)malloc(sizeof(comp_graph_adjacent_t));
		new_connection->element_ptr = (comp_graph_adjacent_t*)adj_node;
		
		if(ptaux != NULL){
			if(prev == NULL){//É o primeiro elemento...
				new_connection->next = ptaux;
				node->list_adjacent_nodes = new_connection;
			}
			else{
				prev->next = new_connection;
				new_connection->next = ptaux;
			}
		}
		else{
			if(prev != NULL){
				prev->next = new_connection;
			}
			else{
				new_connection->next = NULL;
				node->list_adjacent_nodes = new_connection;
			}
		}
		return GRAPH_OP_SUCCESSFUL;
	}
}

int remove_connection_graph(comp_graph_t * node, comp_graph_t * adj_node){
	if(node == NULL || adj_node == NULL){
		if(node == NULL){
			printf("Erro: nodo nulo para remocao!\n");
		}
		if(adj_node == NULL){
			printf("Erro: nodo adjacente nulo para remocao!\n");
		}
		return GRAPH_OP_UNSUCCESSFUL;
	}
	else{
		comp_graph_adjacent_t * ptaux = node->list_adjacent_nodes,*prev = NULL;
		while(ptaux != NULL && (comp_graph_t *)ptaux->element_ptr != adj_node){
			prev = ptaux;
			ptaux = ptaux->next;
		}
	
		if(ptaux != NULL){
			if(prev == NULL){//É o primeiro elemento...
				node->list_adjacent_nodes = ptaux->next;
			}
			else{
				prev->next = ptaux->next;
			}
			free(ptaux->element_ptr);
			free(ptaux);
		}
		else{
			printf("Elemento nao encontrado para remocao (Link %s -> %s)\n", node->name, adj_node->name);	
		}
		return GRAPH_OP_SUCCESSFUL;
	}
}


comp_graph_t * lookup_element_graph(int searched_id,comp_graph_t * graph_lst){
	comp_graph_t *ptaux = graph_lst;
	while(ptaux != NULL && searched_id != ptaux->id){
		ptaux = ptaux->next;
	}
	return ptaux;
}


comp_graph_t * remove_element_graph(comp_graph_t * graph_lst, char * name){

	comp_graph_t * ptaux = graph_lst, * prev = NULL;
	if(graph_lst == NULL){
		printf("Error: graph list has not been initialized!\n");
		return NULL;
	}
	if(graph_lst->id == -1 && graph_lst->next == NULL){//Grafo nao teve a primeira entrada preenchida ainda...
		printf("Error: graph list is empty!\n");
		return graph_lst;
	}
	while(ptaux->next != NULL && strcmp(ptaux->name,name) != 0){
		prev = ptaux;
		ptaux = ptaux->next;
	}

	if(ptaux != NULL && strcmp(ptaux->name,name) == 0){
		if(prev == NULL){//Caso estivermos na primeira entrada...
			comp_graph_t * toReturn = ptaux->next;
			delete_connections(ptaux->list_adjacent_nodes);
			free(ptaux);
			return toReturn;
		}
	
		prev->next = ptaux->next;
		delete_connections(ptaux->list_adjacent_nodes);
		free(ptaux);
		return prev;	
	}
	else{
		printf("Did not find element for removal!\n");
	}
	return graph_lst;
}



void delete_connections(comp_graph_adjacent_t * lst){
	if(lst != NULL){
		delete_connections(lst->next);
		free(lst);
	}
}










